organization := "com.moneyfarm"

name := "sample-forking-project"

version := "0.1"

scalaVersion := "2.13.8"

val akkaHttpVersion = "10.2.9"
val akkaVersion = "2.6.20"
val logbackVersion      = "1.2.7"
val scalaLoggingVersion = "3.9.4"

libraryDependencies ++= Seq(
"com.typesafe.akka" %% "akka-http" % akkaHttpVersion,
"com.typesafe.akka" %% "akka-actor" % akkaVersion,
"com.typesafe.akka" %% "akka-stream" % akkaVersion,
"ch.qos.logback" % "logback-classic" % logbackVersion,
"com.typesafe.scala-logging" %% "scala-logging" % scalaLoggingVersion

)
