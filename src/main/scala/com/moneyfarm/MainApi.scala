package com.moneyfarm

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.typesafe.scalalogging.LazyLogging

import scala.util.{Failure, Success}

object MainApi extends LazyLogging {

  val route: Route = path("ping") {
    get {
      logger.debug(s"GET /ping")
      complete("pong")
    }
  }

  def main(args: Array[String]): Unit = {
    logger.info("Starting service ...")

    implicit val system = ActorSystem("sample-project")
    import system.dispatcher

    val host: String = "0.0.0.0"
    val port: Int = 8080

    Http()
      .newServerAt(host, port)
      .bind(route)
      .onComplete {
        case Success(_) => logger.info(s"Started at port $port")
        case Failure(e) => logger.error("Failed to start", e)
      }
  }
}
